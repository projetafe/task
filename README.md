**TASK**

## Auteurs

Florian Ledolley
Antoine Le Henaff
Edouard Bracquemond


## Fonctionnement de l'application

L'écran d'accueil affiche la liste des tâches,nom de chaque tâche, description et priorité.

Chaque tâche a un fond coloré:

1) Blanc : Indique que la tâche est dans un état normal
2) Vert : La tâche a été terminée dans les temps
3) Rouge : La deadline de la tâche est dépassée alors que la tâche n'est pas terminée.
4) Jaune : La deadline est dépassée mais la tâche est terminée

Le bouton "Ajouter une nouvelle tâche" envoie vers l'activité de création de tâche.

Sur l'écran de création de tâche, il est possible de renseigner:

1) un nom
2) une description
3) une priorité entre : High, Default et Low
4) une date de début
5) une date de fin

Les choix de date nous envoient vers un écran pour choisir la date et l'heure. Si l'on choisit de changer l'heure ou la date , une horloge ou un calendrier s'ouvre.

Une fois tous les champs renseigné, on peut ajouter la tâche, l'application nous renvoie vers la page d'accueil ou notre tâche apparait.

Dans la liste, si l'on appuye sur une tâche, l'application nous envoie vers l'écran de modification d'une tâche, on peut ainsi:

1) modifier les attributs de la tâche.
2) supprimer la tâche
3) terminer la tâche (si la tâche est déjà terminé il est possible de faire à nouveau terminer pour annuler)

Lorsque la deadline d'une tâche approche, une notification nous alerte (30 min avant la fin) ainsi que lorsque le temps est écoulé.








